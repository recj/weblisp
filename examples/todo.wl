(body

    (p "hello, " (env "USER"))

    (b "TODO:")
    (ul
        (li (st "argument parser"))
        (li "CSS expressions")
        (li "JSON expressions")
        (li "file import")
        (li "CGI string parser & CGI expression")
        (li (st "if statement"))
        (li "case statement?")
        (li "moar tests")
        (li "build cmd")
        (li "def expression/variable storage"))
    (img "https://pbs.twimg.com/media/CUNZWTTUcAAicyv.png" 
         (width 100)))
