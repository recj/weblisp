/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __TEST__
#define __TEST__

#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>
#include "parse.h"
#include "utils/stripstr.h"
#include "args/args.h"
#include "utils/hash.h"


#define TEST_BEGIN(...)\
int main() {\
    printf("Begin " __FILE__ ": %s\n", STRINGIFY(__VA_ARGS__));\
    unsigned int ___test_num = 0;\
    unsigned int ___fail_count = 0;


#define TEST_END\
    ___TEST_END:\
    printf("End " __FILE__ ": %d run, %d passed, %d failed\n", ___test_num, ___test_num - ___fail_count, ___fail_count);\
    return ___fail_count;\
}

#define test_assert(op)\
    do {\
        printf("Test " __FILE__ ":" STRINGIFY(__LINE__) " %d (%s)... ", ++___test_num, STRINGIFY(op));\
        fflush(stdout);\
        if ((op) == 0) {\
            ___fail_count += 1;\
            printf("\033[31mfail\033[0m\n");\
        } else {\
            printf("\033[32mpass\033[0m\n");\
        }\
    } while (0)

#define test_asserteq(a,b)\
    do {\
        printf("Test " __FILE__ ":" STRINGIFY(__LINE__) " %d (%s) == (%s)... ", ++___test_num, STRINGIFY(a), STRINGIFY(b));\
        fflush(stdout);\
        __auto_type aa = a;\
        __auto_type bb = b;\
        if (aa != bb) {\
            ___fail_count += 1;\
            printf("\033[31mfail\033[0m (%d != %d)\n", aa, bb);\
        } else {\
            printf("\033[32mpass\033[0m\n");\
        }\
    } while (0)

#define STOP_IF_FAILING\
    do {\
        if (___fail_count) goto ___TEST_END;\
    } while (0)


#endif
