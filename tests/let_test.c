/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "test.h"

TEST_BEGIN(let expressions)
    ArgOptions opts = {
        .pre = 0,
        .run = 1
    };
    
    char str[] =
    "(let x 10)\n"
    "(let y 10)\n"
    "(= x 10)\n"
    "(= 10 x)\n"
    "(= x x)\n"
    "(= x y)\n"
    "(let (testfunc aa) (if aa \"asdf\" x))"
    "(testfunc true)\n"
    "(testfunc false)\n"
    ;
    
    DataType const correcttype[] = {
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_FUNCTION,
        DATATYPE_STRING,
        DATATYPE_INTEGER
    };
    
    unsigned long long correctval[] = {
        10,
        10,
        DATABOOL_TRUE,
        DATABOOL_TRUE,
        DATABOOL_TRUE,
        DATABOOL_TRUE,
        0,
        0,
        10
    };
    
    Token *const head = tokenizer_run(str);

    test_assert(head != NULL);

    Token *tok = head;
    Data *result = stacker_run(tok, &opts, callstack_init());
    Data *dat = result;
    test_assert(result != NULL);
    
    /* data comes out in reverse order! */
    int i = (sizeof(correcttype) / sizeof(correcttype[0]));
    
    while (dat != NULL) {
        --i;
        test_asserteq(dat->type, correcttype[i]);
        
        switch (dat->type) {
        case DATATYPE_BOOL:
            test_asserteq(dat->as_bool, correctval[i]);
            break;
        
        case DATATYPE_INTEGER:
            test_asserteq(dat->as_int, correctval[i]);
            break;
            
        default:
            break;
        }
        
        dat = dat->next;
    }
    
    data_free(dat);
    tokenizer_free(head);
TEST_END
