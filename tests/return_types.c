/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "test.h"

TEST_BEGIN(verify datatypes for values)
    ArgOptions opts = {
        .pre = 0,
        .run = 1
    };
    
    char str[] =
    "true\n"
    "false\n"
    "(= 1 1)\n"
    "\n"
    "\"hello world\"\n"
    "\"\\\\\"\n"  /* tokenizer sees "\\" */
    "\n"
    "(px 100)\n"
    "\n"
    "(foo bar)\n"
    "(foo bar bat baz \"asdf\" 2004.324)\n"
    "\n"
    "1\n"
    "-1\n"
    "+1\n"
    "0x123D\n"
    "0xA123\n"
    "0b0110\n"
    "\n"
    "0.5\n"
    "5.\n"
    "-3.5\n"
    "+3.141\n"
    "\n"
    "(raw \"<p>hello, world!</p>\")\n"
    "(raw \"\\\\\\\"\\\\\")\n" /* tokenizer sees "\\\"\\" */
    "(width 100)\n"
    "(width (px 100))\n"
    "(height (px 100))\n"
    "(height 100)\n"
    "(list 10 20 30 40 50)"
    "(list (list 10 20 30 40 50) (list \"hello\" \"world!\"))"
    ;
    
    DataType const correcttype[] = {
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        
        DATATYPE_STRING,
        DATATYPE_STRING,
        
        DATATYPE_DIMENSION_PX,
        
        DATATYPE_CALL,
        DATATYPE_CALL,
        
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        
        DATATYPE_FLOATING,
        DATATYPE_FLOATING,
        DATATYPE_FLOATING,
        DATATYPE_FLOATING,
        
        DATATYPE_RAW,
        DATATYPE_RAW,
        
        DATATYPE_WIDTH,
        DATATYPE_WIDTH,
        DATATYPE_HEIGHT,
        DATATYPE_HEIGHT,
        
        DATATYPE_LIST,
        DATATYPE_LIST,
    };
    
    Token *head = tokenizer_run(str);
    test_assert(head != NULL);
    STOP_IF_FAILING;
    
    Data *dat = stacker_run(head, &opts, callstack_init());
    Data *data = dat;
    
    test_assert(data != NULL);
    
    int i = sizeof(correcttype) / sizeof(correcttype[0]);
    
    while (data != NULL) {
        --i;
        test_asserteq(data->type, correcttype[i]);
        data = data->next;
    }
    
    data_free(dat);
    tokenizer_free(head);
TEST_END
