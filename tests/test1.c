/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "test.h"

TEST_BEGIN(basic equality expressions, if statements)
    ArgOptions opts = {
        .pre = 0,
        .run = 1
    };
    
    char str[] =
    "(= 2 2)\n"
    "(= 2 2 2)\n"
    "(= 2 2 2 2)\n"
    "(= 2 2 2 3)\n"
    "(if true 0 1)\n"
    "(if false 0 123)\n"
    "(= \"hello\" \"hello\")\n"
    "(= \"hello\" \"world\")\n"
    "(= \"hello\" \"world\" \"hello\")\n"
    "(if (= 2 2) true false)\n"
    "(if (= 2345 2) true false)\n"
    "(if (= 2345 2) 1 false)\n"
    "(if true 919 false)\n"
    ;
    
    unsigned long long const correctval[] = {
        DATABOOL_TRUE,
        DATABOOL_TRUE,
        DATABOOL_TRUE,
        DATABOOL_FALSE,
        0,
        123,
        DATABOOL_TRUE,
        DATABOOL_FALSE,
        DATABOOL_FALSE,
        DATABOOL_TRUE,
        DATABOOL_FALSE,
        DATABOOL_FALSE,
        919,
    };
    
    DataType const correcttype[] = {
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_INTEGER,
        DATATYPE_INTEGER,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_BOOL,
        DATATYPE_INTEGER,
    };
    
    Token *const head = tokenizer_run(str);

    test_assert(head != NULL);

    Token *tok = head;
    Data *result = stacker_run(tok, &opts, callstack_init());
    Data *dat = result;
    test_assert(result != NULL);
    
    /* data comes out in reverse order! */
    int i = (sizeof(correcttype) / sizeof(correcttype[0]));
    
    while (dat != NULL) {
        --i;
        test_assert(dat->type == correcttype[i]);
        
        switch (dat->type) {
        case DATATYPE_BOOL:
            test_assert(dat->as_bool == correctval[i]);
            break;
        
        case DATATYPE_INTEGER:
            test_assert(dat->as_int == correctval[i]);
        }
        
        dat = dat->next;
    }
    
    data_free(result);
    tokenizer_free(head);
TEST_END

