#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include "parse.h"
#include "stack.h"
#include "onpop.h"
#include "onpush.h"
#include "definition.h"
#include "utils/errmsg.h"

static inline Data *push_data(Data *restrict stk, Data *restrict data)
{
    assert(data != NULL);
    data->next = stk;
    return data;
}

/* HACK: allows preservation of ArgOptions across calls to stacker_run.
this has to be done because onpop() functions related to user-defined
variables/functions need to be able to call stacker_run, but do not have
access to the argopts. */
/* TODO: find a cleaner way to store argopts across function calls without
introducing shared state between the args.c and stack.c */
static inline ArgOptions *preserve_argopts(ArgOptions *opts)
{
    static ArgOptions *preserved = NULL;
    if (opts) preserved = opts;
    return preserved;
}

static inline Data *makecall(int argc, Data *argv, Token *tok)
{
    Data *out = data_init();
    out->type = DATATYPE_CALL;
    out->as_call.argc = argc;
    out->as_call.argv = argv;
    out->token = tok;
    return out;
}

Data *eval_args(Token *it, CallStack *cstk, int *err)
{
    Data *args = NULL;
    while (it != NULL) {
        unsigned int flags = parsers[it->id].flags;
        Data *pushdat = NULL;
        
        if (flags & TPFLAGS_PUSH) {
            pushdat = eval_dispatcher(it, cstk);
            /* advance to next expr, ignoring nested */
            unsigned int i = 1;
            do {
                it = it->next;
                if (parsers[it->id].flags & TPFLAGS_PUSH) i += 1;
                else if (parsers[it->id].flags & TPFLAGS_POP) i -= 1;
            } while (i);
            cstk->argc += 1;
        
        } else if (flags & TPFLAGS_ATOM) {
            assert(parsers[it->id].onpush != NULL);
            pushdat = parsers[it->id].onpush(it, cstk);
            cstk->argc += 1;
        
        } else if (flags & TPFLAGS_FUNC) {
            errmsg_print("illegal position for reserved token", it);
            
        } else if (flags & TPFLAGS_POP) break;
        
        if (pushdat == NULL) {
            data_free(args);
            *err = 1;
            return NULL;
        }
        
        args = push_data(args, pushdat);
        it = it->next;
    }
    
    return args;
}

Data *eval_special(Token *popfn, Token *it, CallStack *cstk)
{
    int err = 0;
    Data *args = eval_args(it, cstk, &err);
    assert(cstk->token == popfn);
    return makecall(cstk->argc, args, popfn);
}

Data *eval_normal(Token *popfn, Token *it, CallStack *cstk)
{
    assert(popfn != NULL);
    assert(parsers[popfn->id].onpop != NULL);
    unsigned int popflags = 0;
    int err = 0;
    
    Data *result = NULL, *args = eval_args(it, cstk, &err);
    if (!err) {
        if ((preserve_argopts(NULL)->pre) && (cstk->nopre))
            result = eval_special(popfn, it, cstk);
        else
            result = parsers[popfn->id].onpop(cstk->token, cstk->argc, args, &popflags, cstk->next);
    }
    
    if (result == NULL) {
        data_free(args);
        return NULL;
    }
    
    if (!(popflags & POPFLAG_DONT_FREE_ARGS)) data_free(args);
    return result;
}

Data *eval_dispatcher(Token *tok, CallStack *cstk)
{
    assert(parsers[tok->id].flags & TPFLAGS_PUSH);
    Token *popfn = tok->next;
    assert(parsers[popfn->id].flags & TPFLAGS_FUNC);
    Token *it = popfn->next;
    
    CallStack new_cstk = {
        .next = cstk,
        .token = popfn,
        .letmember = (cstk->letexp || cstk->letmember),
        .lazymember = (cstk->lazy || cstk->lazymember),
        .nopre = (parsers[popfn->id].flags & TPFLAGS_NOPRE) ? 1 : 0,
        .lazy = (parsers[popfn->id].flags & TPFLAGS_LAZY) ? 1 : 0,
        .letexp = (parsers[popfn->id].flags & TPFLAGS_LET) ? 1 : 0,
        .defs = deftable_init(),
        .argc = 0
    };

    if (new_cstk.nopre)
        for (CallStack *c = cstk; c->token != NULL; c = c-> next) c->nopre = 1;
    
    Data *out;
    if (new_cstk.letmember || new_cstk.lazymember)
        out = eval_special(popfn, it, &new_cstk);
    else out = eval_normal(popfn, it, &new_cstk);
    
    if (!out) {
        hintmsg_print("Error occurred while evaluating this expression", popfn);
    }
    
    deftable_free(new_cstk.defs);
    return out;
}

Data *stacker_run(Token *tok, ArgOptions *opts, CallStack *cstk)
{
    int err = 0;
    preserve_argopts(opts);
    Data *result = eval_args(tok, cstk, &err);
    return result;
}
