/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "parse.h"
#include "utils/lazyregex.h"
#include <stdio.h>

void tokenizer_free(Token *token)
{
    if (token == NULL) return;
    Token *next = token->next;
    free(token);
    if (next != NULL) tokenizer_free(next);
}

static Token *token_init(char const *str, int matchlen, char const *keystr, TokenId id)
{
    Token *out = malloc(sizeof(Token) + (sizeof(str[0]) * matchlen) + 1);
    out->contents = ((char*)out) + sizeof(Token);
    
    strncpy(out->contents, str, matchlen);
    out->contents[matchlen] = 0;
    out->id = id;
    out->keystr = keystr;
    out->lineno = 0;
    out->next = NULL;
    out->prev = NULL;
    
    return out;
}

static Token *parse(char const *str, char const **str_remaining)
{
    for (unsigned int i = 0; i < PARSERS_LENGTH; ++i) {
        if (parsers[i].matchfunc != NULL) {
            int result = parsers[i].matchfunc(str);

            if (result > 0) {
                Token *out = token_init(str, result, parsers[i].keystr, i);
                *str_remaining += result;
                return out;
            }
        }
    }
    
    return NULL;
}

static void parsers_cleanup()
{
    for (unsigned int i = 0; i < PARSERS_LENGTH; ++i) {
        if (parsers[i].onexit != NULL) {
            parsers[i].onexit();
        }
    }
}

Token *tokenizer_run(char const *str)
{
    Token *head = NULL;
    Token *tail = NULL;
    char const *remain = str;
    unsigned int lineno = 1;
    
    while (remain[0] != 0) {
        Token *newtok = parse(str, &remain);
        
        if (newtok == NULL) {
            fprintf(stderr, "Tokenizer error at line %u:\n", lineno);
            if (str[0] < ' ') fprintf(stderr, "    unexpected control byte 0x%02x\n", str[0]);
            else fprintf(stderr, "    character '%c' (0x%02x) did not match any known syntax\n", str[0], str[0]);
            
            break;
        }
        
        newtok->lineno = lineno;
        
        char *c = newtok->contents;
        do {
            if (*c == '\n') ++lineno;
        } while (*(++c) != 0);
        
        if ((parsers[newtok->id].flags & TPFLAGS_DISCARD) == 0) {
            if (head == NULL) {
                head = newtok;
                tail = newtok;
            
            } else {
                newtok->prev = tail;
                tail->next = newtok;
                tail = newtok;
            }
        }
        
        str = remain;
    }
    
    parsers_cleanup();
    
    if (remain[0] != 0) {
        if (head != NULL) tokenizer_free(head);
        return NULL;
    
    } else return head;
}
