/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "cgi/cgi.h"
#include "utils/dynstring.h"
#include "stack.h"
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>

static inline int cgi_put(CgiPair *pairs, String *key, String *val, int idx)
{
    for (int i = 0; i <= idx; ++i) {
        if (pairs[i].key && !strcmp(pairs[i].key, string_ascharstr(key))) {
            if (pairs[i].value->type == DATATYPE_STRING) {
                Data *conv = data_init();
                conv->type = DATATYPE_LIST;
                conv->as_list.data = pairs[i].value;
                pairs[i].value = conv;
            }
            assert(pairs[i].value->type == DATATYPE_LIST);
            Data *nw = data_init();
            nw->type = DATATYPE_STRING;
            nw->as_string = string_ascharstr(val);
            nw->next = pairs[i].value->as_list.data;
            pairs[i].value->as_list.data = nw;
            return idx;
        }
    }
    Data *nw = data_init();
    nw->type = DATATYPE_STRING;
    nw->as_string = string_ascharstr(val);
    pairs[idx].value = nw;
    pairs[idx].key = string_ascharstr(key);
    return idx + 1;
}

/* var1=value1&var2=with%20percent%20encoding */
static int cgi_parsequerystr(CgiPair *pairs, char const *str, int const len, int const idx)
{
    assert(pairs && str);
    if (idx >= len) return -1;

    unsigned int eq = 0;
    while (str[eq] && (str[eq] != '=')) ++eq;
    if (!str[eq]) return -1;

    unsigned int dlim = eq;
    while (str[dlim] && (str[dlim] != '&')) ++dlim;

    String k, v;
    string_init(&k);
    string_init(&v);
    string_catbytes(&k, str, eq);
    string_catbytes(&v, str + eq + 1, dlim - (eq + 1));
    if (string_unpackreqesc(&k)) return -2;
    if (string_unpackreqesc(&v)) return -2;
    unsigned int nidx = cgi_put(pairs, &k, &v, idx);
    
    if (str[dlim]) return cgi_parsequerystr(pairs, str + dlim + 1, len, nidx);
    else return 0;
}

RequestType cgi_reqtype()
{
    char const *env = getenv("REQUEST_METHOD");
    
    if (!env) return REQTYPE_UNKNOWN;
    else if (!strcmp(env, "GET")) return REQTYPE_GET;
    else if (!strcmp(env, "POST")) return REQTYPE_POST;
    
    return REQTYPE_UNKNOWN;
}

CgiPair *cgi_getparams()
{
    String s;
    RequestType req = cgi_reqtype();
    
    if (req == REQTYPE_GET) {
        string_init(&s);
        char *cs = getenv("QUERY_STRING");
        if (cs) string_catbytes(&s, cs, strlen(cs));
    
    } else if (req == REQTYPE_POST) {
        string_init(&s);
        int c = 0;
        while ((c = fgetc(stdin)) != EOF) {
            char const ch = (c & 0xFF);
            string_catbytes(&s, &ch, 1);
        }
        
    } else return NULL;
    
    int outlen = string_countchar(&s, '&');
    if ((outlen < 0) || (outlen > (INT_MAX - 1))) {
        string_free(&s);
        return NULL;
    }
    
    CgiPair *out = calloc(outlen + 1, sizeof(CgiPair));
    if (!out) {
        string_free(&s);
        return NULL;
    }
    
    out[outlen].key = NULL;
    out[outlen].value = NULL;
    
    assert(cgi_parsequerystr(out, string_ascharstr(&s), outlen + 1, 0) == 0);
    
    return out;
}
