/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "parse.h"
#include "onpush.h"
#include "onpop.h"
#include "utils/lazyregex.h"
#include <stdio.h>

/* lazy_static parsers that are part of the parsers table */
LAZY_STATIC_REGEX_PARSER(comment, "^;;[[:print:]]*[\n\r\v\f]+");
LAZY_STATIC_REGEX_PARSER(shebang, "^#![[:print:]]*[\n\r\v\f]+");
LAZY_STATIC_REGEX_PARSER(intbase10, "^[-+]{0,1}[[:digit:]]+");
LAZY_STATIC_REGEX_PARSER(intbase16, "^0x[[:xdigit:]]+");
LAZY_STATIC_REGEX_PARSER(intbase2, "^0b[01]+");
LAZY_STATIC_REGEX_PARSER(floating, "^[-+]{0,1}[[:digit:]]+\\.[[:digit:]]*");
LAZY_STATIC_REGEX_PARSER(bool_helper, "^(true|false)([()[:space:]])");
LAZY_STATIC_REGEX_PARSER(whitespace, "^[[:space:]]*");
LAZY_STATIC_REGEX_PARSER(head, "^(head)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(body, "^(body)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(address, "^(address)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(article, "^(article)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(aside, "^(aside)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(code, "^(code)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(cite, "^(cite)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(details, "^(details)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(figcaption, "^(figcaption)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(figure, "^(figure)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(footer, "^(footer)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(header, "^(header)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h1, "^(h1)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h2, "^(h2)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h3, "^(h3)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h4, "^(h4)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h5, "^(h5)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(h6, "^(h6)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(main_, "^(main)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(mark, "^(mark)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(nav, "^(nav)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(section, "^(section)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(summary, "^(summary)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(title, "^(title)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(time, "^(time)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(b, "^(b|strong)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(i, "^(i|em)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(paragraph, "^(p)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(px, "^(px)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(ul, "^(ul)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(ol, "^(ol)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(st, "^(st)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(li, "^(li)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(eq, "^(=)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(fplus, "^[+][[:space:]]");
LAZY_STATIC_REGEX_PARSER(fminus, "^[-][[:space:]]");
LAZY_STATIC_REGEX_PARSER(fdiv, "^[/][[:space:]]");
LAZY_STATIC_REGEX_PARSER(fmult, "^[*][[:space:]]");
LAZY_STATIC_REGEX_PARSER(iff, "^(if)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(env, "^(env)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(img, "^(img)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(width, "^(width)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(height, "^(height)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(list, "^(list)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(cons, "^(cons)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(let, "^(let)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(style, "^(style)[[:space:]]");
LAZY_STATIC_REGEX_PARSER(assert_, "^(assert)[[:space:]]");

/* NOT in the parsers table, but used as helpers for other parsers */
LAZY_STATIC_REGEX_PARSER(barewords_helper, "^([^[:space:][:punct:]]|[_-])+([()[:space:]])");
LAZY_STATIC_REGEX_PARSER(raw_helper, "^(raw)[[:space:]]+");

static int openparen(char const *str) { return str[0] == '('; }
static int closeparen(char const *str) { return str[0] == ')'; }
static int bareword(char const *str)
{
    int result = barewords_helper(str);
    /* regex should always fail or match at least 2 chars
     (a word then a whitespace or paren). This might also get
     triggered when the regex fails to compile. */
    assert(result != 1);
    
    if (result > 1) return result - 1;
    else return 0;
}

static int boolean(char const *str)
{
    int result = bool_helper(str);
    if (result <= 0) return result - 1;
    else return result - 1;
}

static int string(char const *str)
{
    if ((str[0] != '"') || (str[1] == 0)) return 0;
    
    int n = 1;
    int nesc = 1;
    while (str[n]) {
        if ((str[n] == '"') && (nesc)) {
            return n + 1;
        
        } else if ((str[n] == '\\')  && (nesc)) {
            nesc = 0;
        
        } else nesc = 1;
        
        ++n;
    }
    
    return 0;
}

static int raw(char const *str)
{
    int result = raw_helper(str);
    assert(result != 1);
    if (result <= 0) return 0;
    
    int s = string(str + result);
    if (s <= 0) return 0;
    else return result + s;
}

/* TPFLAGS_DISCARD are immediately discarded by the tokenizer, stack machine will (should) never see them */
/* NOTE: very slow now that there's a lot of parsers. may be worth looking into other ways to do tokenization */
TokenParser const parsers[] = 
{                     /* matchfunc   keystr              flags     onexit             onpush             onpop */
[TOKENID_COMMENT] =    { comment,    "",          TPFLAGS_DISCARD, comment_atexit,     NULL,              NULL },
[TOKENID_SHEBANG] =    { shebang,    "",          TPFLAGS_DISCARD, shebang_atexit,     NULL,              NULL },
[TOKENID_WHITESPACE] = { whitespace, "Blank",     TPFLAGS_DISCARD, whitespace_atexit,  NULL,              NULL },

[TOKENID_OPENPAREN] =  { openparen,  "(",            TPFLAGS_PUSH, NULL,               NULL,              NULL },
[TOKENID_CLOSEPAREN] = { closeparen, ")",            TPFLAGS_POP,  NULL,               NULL,              NULL },

[TOKENID_STRING] =     { string,     "String",       TPFLAGS_ATOM, NULL,               ONPUSH(string),    NULL },
[TOKENID_FLOATING] =   { floating,   "Floating",     TPFLAGS_ATOM, floating_atexit,    ONPUSH(floating),  NULL },
[TOKENID_INT_BASE2] =  { intbase2,   "IntLitBase2",  TPFLAGS_ATOM, intbase2_atexit,    ONPUSH(intbase2),  NULL },
[TOKENID_INT_BASE10] = { intbase10,  "IntLitBase10", TPFLAGS_ATOM, intbase10_atexit,   ONPUSH(intbase10), NULL },
[TOKENID_INT_BASE16] = { intbase16,  "IntLitBase16", TPFLAGS_ATOM, intbase16_atexit,   ONPUSH(intbase16), NULL },
[TOKENID_BOOL] =       { boolean,    "Boolean",      TPFLAGS_ATOM, bool_helper_atexit, ONPUSH(boolean),   NULL },
[TOKENID_RAW] =        { raw,        "raw",          TPFLAGS_FUNC, raw_helper_atexit,  NULL,        ONPOP(raw) },

[TOKENID_STYLE] =      { style,      "style",        TPFLAGS_FUNC, style_atexit,      NULL,       ONPOP(style) },
[TOKENID_LIST] =       { list,       "list",         TPFLAGS_FUNC, list_atexit,       NULL,        ONPOP(list) },
[TOKENID_CONS] =       { cons,       "cons",         TPFLAGS_FUNC, cons_atexit,       NULL,        ONPOP(cons) },
[TOKENID_HEAD] =       { head,       "head",         TPFLAGS_FUNC, head_atexit,       NULL,        ONPOP(head) },
[TOKENID_BODY] =       { body,       "body",         TPFLAGS_FUNC, body_atexit,       NULL,        ONPOP(body) },
[TOKENID_PARAGRAPH] =  { paragraph,  "p",            TPFLAGS_FUNC, paragraph_atexit,  NULL,           ONPOP(p) },
[TOKENID_SECTION] =    { section,    "section",      TPFLAGS_FUNC, section_atexit,    NULL,     ONPOP(section) },
[TOKENID_ARTICLE] =    { article,    "article",      TPFLAGS_FUNC, article_atexit,    NULL,     ONPOP(article) },
[TOKENID_ASIDE] =      { aside,      "aside",        TPFLAGS_FUNC, aside_atexit,      NULL,       ONPOP(aside) },
[TOKENID_ADDRESS] =    { address,    "address",      TPFLAGS_FUNC, address_atexit,    NULL,     ONPOP(address) },
[TOKENID_DETAILS] =    { details,    "details",      TPFLAGS_FUNC, details_atexit,    NULL,     ONPOP(details) },
[TOKENID_FIGURE] =     { figure,     "figure",       TPFLAGS_FUNC, figure_atexit,     NULL,      ONPOP(figure) },
[TOKENID_FIGCAPTION] = { figcaption, "figcaption",   TPFLAGS_FUNC, figcaption_atexit, NULL,  ONPOP(figcaption) },
[TOKENID_MAIN] =       { main_,      "main",         TPFLAGS_FUNC, main__atexit,      NULL,        ONPOP(main) },
[TOKENID_MARK] =       { mark,       "mark",         TPFLAGS_FUNC, mark_atexit,       NULL,        ONPOP(mark) },
[TOKENID_HEADER] =     { header,     "header",       TPFLAGS_FUNC, header_atexit,     NULL,      ONPOP(header) },
[TOKENID_FOOTER] =     { footer,     "footer",       TPFLAGS_FUNC, footer_atexit,     NULL,      ONPOP(footer) },
[TOKENID_H1] =         { h1,         "h1",           TPFLAGS_FUNC, h1_atexit,         NULL,          ONPOP(h1) },
[TOKENID_H2] =         { h2,         "h2",           TPFLAGS_FUNC, h2_atexit,         NULL,          ONPOP(h3) },
[TOKENID_H3] =         { h3,         "h3",           TPFLAGS_FUNC, h3_atexit,         NULL,          ONPOP(h3) },
[TOKENID_H4] =         { h4,         "h4",           TPFLAGS_FUNC, h4_atexit,         NULL,          ONPOP(h4) },
[TOKENID_H5] =         { h5,         "h5",           TPFLAGS_FUNC, h5_atexit,         NULL,          ONPOP(h5) },
[TOKENID_H6] =         { h6,         "h6",           TPFLAGS_FUNC, h6_atexit,         NULL,          ONPOP(h6) },
[TOKENID_SUMMARY] =    { summary,    "summary",      TPFLAGS_FUNC, summary_atexit,    NULL,     ONPOP(summary) },
[TOKENID_NAV] =        { nav,        "nav",          TPFLAGS_FUNC, nav_atexit,        NULL,         ONPOP(nav) },
[TOKENID_CODE] =       { code,       "code",         TPFLAGS_FUNC, code_atexit,       NULL,        ONPOP(code) },
[TOKENID_CITE] =       { cite,       "cite",         TPFLAGS_FUNC, cite_atexit,       NULL,        ONPOP(cite) },
[TOKENID_TITLE] =      { title,      "title",        TPFLAGS_FUNC, title_atexit,      NULL,       ONPOP(title) },
[TOKENID_TIME] =       { time,       "time",         TPFLAGS_FUNC, time_atexit,       NULL,        ONPOP(time) },

[TOKENID_ST] =         { st,         "st",           TPFLAGS_FUNC, st_atexit,         NULL,          ONPOP(st) },
[TOKENID_PX] =         { px,         "px",           TPFLAGS_FUNC, px_atexit,         NULL,          ONPOP(px) },
[TOKENID_LI] =         { li,         "li",           TPFLAGS_FUNC, li_atexit,         NULL,          ONPOP(li) },
[TOKENID_UL] =         { ul,         "ul",           TPFLAGS_FUNC, ul_atexit,         NULL,          ONPOP(ul) },
[TOKENID_OL] =         { ol,         "ol",           TPFLAGS_FUNC, ol_atexit,         NULL,          ONPOP(ol) },
[TOKENID_B] =          { b,          "b",            TPFLAGS_FUNC, b_atexit,          NULL,           ONPOP(b) },
[TOKENID_I] =          { i,          "i",            TPFLAGS_FUNC, i_atexit,          NULL,           ONPOP(i) },
[TOKENID_EQ] =         { eq,         "=",            TPFLAGS_FUNC, eq_atexit,         NULL,          ONPOP(eq) },
[TOKENID_PLUS] =       { fplus,      "-",            TPFLAGS_FUNC, fplus_atexit,      NULL,       ONPOP(fplus) },
[TOKENID_MINUS] =      { fminus,     "+",            TPFLAGS_FUNC, fminus_atexit,     NULL,      ONPOP(fminus) },
[TOKENID_ASTERISK] =   { fmult,      "*",            TPFLAGS_FUNC, fmult_atexit,      NULL,       ONPOP(fmult) },
[TOKENID_FWDSLASH] =   { fdiv,       "/",            TPFLAGS_FUNC, fdiv_atexit,       NULL,        ONPOP(fdiv) },
[TOKENID_WIDTH] =      { width,      "width",        TPFLAGS_FUNC, width_atexit,      NULL,       ONPOP(width) },
[TOKENID_HEIGHT] =     { height,     "height",       TPFLAGS_FUNC, height_atexit,     NULL,      ONPOP(height) },
[TOKENID_IF] =         { iff,        "if", TPFLAGS_LAZY|TPFLAGS_FUNC, iff_atexit,     NULL,         ONPOP(iff) },
[TOKENID_IMG] =        { img,        "img",          TPFLAGS_FUNC, img_atexit,        NULL,         ONPOP(img) },

[TOKENID_ASSERT] =     { assert_,    "assert",       TPFLAGS_FUNC,assert__atexit,     NULL,      ONPOP(assert) },

[TOKENID_LET] =        { let,        "let", TPFLAGS_FUNC|TPFLAGS_LET,let_atexit,      NULL,         ONPOP(let) },
[TOKENID_ENV] =        { env,        "env", TPFLAGS_FUNC|TPFLAGS_NOPRE,env_atexit,    NULL,         ONPOP(env) },
[TOKENID_BAREWORD] =   { bareword, "Symbol", TPFLAGS_FUNC|TPFLAGS_ATOM, 
                                                     barewords_helper_atexit, ONPUSH(bareword), ONPOP(bareword) },
};
