/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "onpop/onpop_private.h"

char *flatten_dataarr_html(Data const **arrdat, unsigned int len)
{
    int error = 0;
    char buff[32] = {};
    String cat;
    string_init(&cat);
    for (unsigned int i = 0; (i < len) && (!error); ++i) {
        if (arrdat[i]->type == DATATYPE_STRING) {
            String sc;
            string_init(&sc);
            string_catbytes(&sc, arrdat[i]->as_string, strlen(arrdat[i]->as_string));
            string_packhtmlesc(&sc);
            string_catbytes(&cat, string_ascharstr(&sc), string_bytelen(&sc));
            string_free(&sc);
            
        } else if (arrdat[i]->type == DATATYPE_RAW) {
            string_catbytes(&cat, arrdat[i]->as_string, strlen(arrdat[i]->as_string));
            
        } else if (arrdat[i]->type == DATATYPE_LIST) {
            char *k = flatten_list_html(arrdat[i]);
            if (k) {
                string_catbytes(&cat, k, strlen(k));
                free(k);
            } else error = 1;
            
        } else if (arrdat[i]->type == DATATYPE_BOOL) {
            if (arrdat[i]->as_bool == DATABOOL_TRUE) string_catbytes(&cat, "true", 4);
            else string_catbytes(&cat, "false", 5);
            
        } else if (arrdat[i]->type == DATATYPE_FLOATING) {
            int l = snprintf(buff, sizeof(buff), "%g", arrdat[i]->as_floating);
            string_catbytes(&cat, buff, l);
            
        } else if (arrdat[i]->type == DATATYPE_INTEGER) {
            printf("PROCESSING AS INT\n");
            int l = snprintf(buff, sizeof(buff), "%i", arrdat[i]->as_int);
            printf("%d\n", l);
            string_catbytes(&cat, buff, l + 1);
            
        } else {
            errmsg_print("type cannot be stringified", arrdat[i]->token);
            error = 1;
        }
    }
    
    if (error) {
        string_free(&cat);
        return NULL;
    } else return string_ascharstr(&cat);
}

char *flatten_list_html(Data const *lstdat)
{
    assert(lstdat);
    assert(lstdat->type == DATATYPE_LIST);
    String cat;
    string_init(&cat);
    
    unsigned int len = 0;
    for (Data const *it = lstdat->as_list.data; it; it = it->next) ++len;
    if (!len) return "";
    
    Data const *members[len];
    unsigned int i = len;
    for (Data const *it = lstdat->as_list.data; i--; it = it->next) members[i] = it;
    
    return flatten_dataarr_html(members, len);
}

Data *ONPOP(assert)(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk)
{
    UNUSED(flags); UNUSED(cstk);
    if (argc != 1 || argv->type != DATATYPE_BOOL) {
        errmsg_print("assert expects one boolean argument", self);
        return NULL;
    
    } else if (argv->as_bool == DATABOOL_FALSE) {
        errmsg_print("assertion failed", self);
        return NULL;
    }
    
    Data *out = data_init();
    out->token = self;
    out->type = DATATYPE_NONE;
    return out;
}
