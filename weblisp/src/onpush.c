/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "onpush.h"
#include "utils/stripstr.h"

Data *ONPUSH(retnull)(Token *self, CallStack const *cstk) { UNUSED(self); UNUSED(cstk); return NULL; }

Data *ONPUSH(bareword)(Token *self, CallStack const *cstk)
{
    assert(self != NULL);
    assert(self->contents != NULL);
    
    Data *ref = callstack_getdef(cstk, self->contents);
    Data *out = data_init();
    if (ref != NULL) {
        memcpy(out, ref, sizeof(Data));
        out->token = self;
        return out;
    }
    
    out->token = self;
    out->type = DATATYPE_SYMBOL;
    
    unsigned int len = strlen(self->contents);
    out->as_string = malloc(sizeof(self->contents[0]) * (len + 1));
    assert(out->as_string != NULL);
    
    strncpy(out->as_string, self->contents, len);
    out->as_string[len] = 0;
    
    return out;
}

Data *ONPUSH(string)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    assert(self != NULL);
    assert(self->contents != NULL);
    
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_STRING;
    unsigned int contlen = strlen(self->contents);
    out->as_string = malloc(sizeof(self->contents[0]) * (contlen + 1));
    assert(out->as_string != NULL);
    stripstr_quots(out->as_string, self->contents, contlen);
    
    return out;
}

Data *ONPUSH(intbase16)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_INTEGER;
    out->as_int = strtol(self->contents, NULL, 16);
    
    return out;
}

Data *ONPUSH(intbase10)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_INTEGER;
    out->as_int = atoi(self->contents);
    
    return out;
}

Data *ONPUSH(floating)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_FLOATING;
    out->as_floating = atof(self->contents);
    
    return out;
}

Data *ONPUSH(intbase2)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_INTEGER;
    out->as_int = strtol(self->contents + 2, NULL, 2);
    
    return out;
}

Data *ONPUSH(boolean)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_BOOL;
    if (strncmp(self->contents, "true", 4) == 0) {
        out->as_bool = DATABOOL_TRUE;
    } else out->as_bool = DATABOOL_FALSE;
    
    return out;
}

Data *ONPUSH(raw)(Token *self, CallStack const *cstk)
{
    UNUSED(cstk);
    Data *out = data_init();
    
    out->token = self;
    out->type = DATATYPE_INTEGER;
    out->as_int = strtol(self->contents + 2, NULL, 2);
    
    return out;
}
