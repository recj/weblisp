/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _STACK_H_
#define _STACK_H_

#include "definition.h"
#include "utils/hash.h"
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum DataBool {
    DATABOOL_FALSE,
    DATABOOL_TRUE
} DataBool;

typedef enum DataType {
    DATATYPE_NONE,
    DATATYPE_STRING,
    DATATYPE_INTEGER,
    DATATYPE_FLOATING,
    DATATYPE_BOOL,
    DATATYPE_RAW,
    DATATYPE_FUNCTION,
    DATATYPE_DIMENSION_PX,
    DATATYPE_AUTO,
    DATATYPE_WIDTH,
    DATATYPE_HEIGHT,
    DATATYPE_LIST,
    DATATYPE_CALL,      /* for any unevaluated expression, e.g. NOPRE's and undefined expressions */
    DATATYPE_SYMBOL     /* for barewords given as expression params */
} DataType;

typedef struct CallStack {
    Token *token;
    unsigned int argc;
    unsigned int nopre  : 1;
    unsigned int lazy   : 1;
    unsigned int lazymember : 1;
    unsigned int letexp : 1;
    unsigned int letmember : 1;
    unsigned int        : 27;
    struct DefTable *defs;
    struct CallStack *next;
} CallStack;

typedef struct Data {
    DataType type;
    union {
        char *as_string;
        int as_int;
        double as_floating;
        uint64_t as_u64;    /* guarantees minimum width of union field */
        DataBool as_bool;
        struct {
            unsigned int argc;
            struct Data *argv;
        } as_call;
        struct {
            char *name;
            struct Data *argnames;
        } as_function;
        struct {
            struct Data *data;
        } as_list;
        struct {
            struct Data *data;
        } as_let;
    };
    unsigned int defrc; /* counts # of references by deftables */
    Token *token;
    struct Data *next;
} Data;

static inline Data *data_init()
{
    Data *out = malloc(sizeof(Data));
    assert(out != NULL);
    
    out->type = DATATYPE_NONE;
    out->as_u64 = 0;
    out->token = NULL;
    out->next = NULL;
    out->defrc = 0;
    
    return out;
}

/* if a and b are strings, it is expected that ahash contains a's hash. */
static inline int data_cmp(Data const *a, Data const *b, unsigned long long ahash)
{
    if (a->type == DATATYPE_FLOATING && b->type == DATATYPE_INTEGER)
        return a->as_floating == b->as_int;
    else if (a->type == DATATYPE_INTEGER && b->type == DATATYPE_FLOATING)
        return a->as_int == b->as_floating;
    else if (b->type == DATATYPE_STRING)
        return ahash == hash_fnv1a_64(b->as_string, -1);
    else
        return a->as_u64 == b->as_u64;
}

CallStack *callstack_init();

void data_free(Data *data);

void callstack_free(CallStack *stk);

Data *callstack_getdef(CallStack const *stk, char const *str);

// Data *eval_normal(Token *popfn, Token *it, CallStack *cstk);

// Data *eval_special(Token *popfn, Token *it, CallStack *cstk);

Data *eval_dispatcher(Token *tok, CallStack *cstk);

#ifdef __cplusplus
}
#endif

#endif
