/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _ONPUSH_H_
#define _ONPUSH_H_

#include "parse.h"
#include "stack.h"
#include <stdio.h>
#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>

#define ONPUSH(name_) name_ ## onpush
#define ONPUSHDECL(name_) name_ ## onpush (Token *self, CallStack const *cstk)

#ifdef __cplusplus
extern "C" {
#endif

Data *ONPUSHDECL(retnull);

Data *ONPUSHDECL(bareword);

Data *ONPUSHDECL(intbase16);

Data *ONPUSHDECL(intbase10);

Data *ONPUSHDECL(intbase2);

Data *ONPUSHDECL(floating);

Data *ONPUSHDECL(string);

Data *ONPUSHDECL(boolean);

#ifdef __cplusplus
}
#endif

#endif
