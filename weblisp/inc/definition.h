/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _DEFINITION_H_
#define _DEFINITION_H_

#include "parse.h"

/* should be a power of two, preferably, but doesn't have to be */
#define DEFINITION_HASHTABLE_SIZE 256

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _STACK_H_
typedef struct Data Data;
#endif

typedef struct Definition {
    char *name;
    Data *data;
    struct Definition *next;
} Definition;

typedef struct DefTable {
    Definition *table[DEFINITION_HASHTABLE_SIZE];
} DefTable;

Definition *definition_create(char const *name, Data *data);

int definition_put(DefTable *table, char const *name, Data *data);

Definition *definition_get(DefTable *table, char const *name);

DefTable *deftable_init();

/* just free the deftable and its child Definitions,
but not the Data-s contained by */
void deftable_free(DefTable *table);

#ifdef __cplusplus
}
#endif

#endif
