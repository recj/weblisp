/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _CGI_H_
#define _CGI_H_

#include "parse.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum RequestType {
    REQTYPE_UNKNOWN,
    REQTYPE_GET,
    REQTYPE_POST,
} RequestType;

typedef struct CgiPair {
    char *key;
    Data *value;
} CgiPair;

RequestType cgi_reqtype();

/* returns heap-allocated array of CgiPairs.
The last pair in the array has a null key and value. */
CgiPair *cgi_getparams();

void cgi_freepairarray(CgiPair *pairarr);

#ifdef __cplusplus
}
#endif

#endif
