/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _PARSE_H_
#define _PARSE_H_

#include "args/args.h"
#include "utils/misc.h"
#include "tokenizer.h"
#include "stack.h"
#include "definition.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int ParseFunction(char const *input);
typedef void OnExit(void);
typedef Data *OnPush(Token *self, CallStack const *cstk);
typedef Data *OnPop(Token *self, int argc, Data *argv, unsigned int *flags, CallStack *cstk);

typedef struct TokenParser {
    ParseFunction *matchfunc;
    char const *keystr;
    unsigned int flags;
    OnExit *onexit;
    OnPush *onpush;
    OnPop *onpop;
} TokenParser;

typedef enum TokenizerMode {
    TOKENIZER_PRE,
    TOKENIZER_RUN
} TokenizerMode;

TokenParser const parsers[PARSERS_LENGTH];

void tokenizer_free(Token *token);

Token *tokenizer_run(char const *str);

Data *stacker_run(Token *tok, ArgOptions *opts, CallStack *cstk);

void stacker_cleanup();

#ifdef __cplusplus
}
#endif

#endif
