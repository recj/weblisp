/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _STRIPSTR_H_
#define _STRIPSTR_H_

#ifdef __cplusplus
extern "C" {
#endif

static inline void stripstr_escapes(char *str, int const len)
{
    assert(str);
    
    if (len <= 0) {
        str[0] = 0;
        return;
    }
    
    unsigned int i = 0, os = 0, esc = 0;
    for (char *c = str; (c < str + len) && (*c != 0); ++c) {
        if (esc) {
            os += 1;
            esc = 0;
            
        } else esc = (*c == '\\');
        
        str[i - os] = *c;
        
        i += 1;
    }
    
    str[i - os] = 0;
}

static inline void stripstr_quots(char *dest, char *src, unsigned int const srclen)
{
    assert(dest);
    assert(src);
    
    if (srclen == 0) {
        dest[0] = 0;
        return;
    }
    
    strncpy(dest, src + 1, srclen - 2);
    dest[srclen - 2] = 0;
}

static inline int str_startswith(char const *prefix, char const *str)
{
    if (!prefix) return 1;
    if (!str) return 0;
    
    char a, b;
    
    while ((b = *str++) && (a = *prefix++)) {
        if (b != a) return 0;
    }
    
    if (!b) return 0;
    
    return 1;
}

#ifdef __cplusplus
}
#endif

#endif
