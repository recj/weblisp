/*
Copyright 2018 Evan Grove

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef _LAZYREGEX_H_
#define _LAZYREGEX_H_

#include <assert.h>
#include <regex.h>
#include <string.h>
#include <stdlib.h>

/* take an array of chars. return the length of the match (<= 0 means no match) */
typedef struct LazyRegex {
    regex_t re;
    int state;
    const char *const restr;
} LazyRegex;

#define LAZY_STATIC_REGEX(_name,_re) static LazyRegex _name = { .state = 0, .restr = _re };

#define LAZY_STATIC_REGEX_PARSER(_name, _re)\
    LAZY_STATIC_REGEX(RE_ ## _name, _re);\
    \
    static int _name(char const *input)\
    {\
        regmatch_t match;\
        if(regexec(lazyregex_get(&(RE_ ## _name)), input, 1, &match, 0)) return 0;\
        else return match.rm_eo;\
    }\
    \
    static void _name ## _atexit() { lazyregex_destroy(&(RE_ ## _name)); }

static inline regex_t *lazyregex_get(LazyRegex *re)
{
    if (re->state == 0) {
        assert(regcomp(&re->re, re->restr, REG_EXTENDED) == 0);
        re->state = 1;
    }
    return &re->re;
}

static inline void lazyregex_destroy(LazyRegex *re)
{
    if (re->state) {
        re->state = 0;
        regfree(&re->re);
    }
}


#endif
