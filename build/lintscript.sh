#!/bin/sh

echo "approximate SLOC:"
CFILES=$(expr `find ../weblisp -type f -name "*.c" | wc -l`)
HFILES=$(expr `find ../weblisp -type f -name "*.h" | wc -l`)
# license info
CFILES_BPLINES=15
# license info, header guards, ifdef __cplusplus
HFILES_BPLINES=24
HLOC=$(($HFILES_BPLINES * $HFILES))
CLOC=$(($CFILES_BPLINES * $CFILES))
LLOC=$(($CLOC + $HLOC))
NLOC=$(grep -rvE "^[[:space:]]*$" ../weblisp  | wc -l)
echo $(($NLOC - $LLOC))

echo ""
echo "finding TODOs:"

grep -rTnE "(//|/\*)[^[:alnum:]]*(BUG|TODO|FIXME|HACK)" ../weblisp

echo ""
echo "cppcheck result:"

cppcheck --quiet --enable=warning --enable=performance --enable=portability ../weblisp
